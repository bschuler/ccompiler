parser:
	gcc Source/*.c Source/*.h parserTest.c -o parserTest

unary:
	gcc Source/*.c Source/*.h unaryTest.c -o unaryTest

compound:
	gcc Source/*.c Source/*.h compoundTest.c -o compoundTest

minus:
	gcc Source/*.c Source/*.h minusTest.c -o minusTest

buffer:
	gcc Source/*.c Source/*.h  bufferTest.c -o bufferTest

lexer:
	gcc Source/*.c Source/*.h lexerTest.c -o lexerTest

symbol:
	gcc Source/*.c Source/*.h symbolTest.c -o symbolTest

ast:
	gcc Source/*.c Source/*.h astTest.c -o astTest

wri:
	gcc Source/*.c Source/*.h writingc.c -o writingc

loop:
	gcc Source/*.c Source/*.h loopFunctionsTest.c -o loopFunctionsTest

clean:
	rm bufferTest parserTest lexerTest symbolTest astTest writingc unaryTest minusTest compoundTest loopFunctionsTest *.py