#include "Source/parser.h"

int main(){
  char* filename = "loopFunctions";
  symbol_t* symbolTable = parser(filename);
  generate(symbolTable, filename);
}