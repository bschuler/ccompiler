# Théorie de la compilation : Compilateur

## Nom(s)

Benoît SCHULER, pas de groupe

## Langages

Source : langage intech
Langage du compilateur : C
Langage cible : python

### Langage INTECH

pas de commentaires
pas de variables globales
2 types : rien et entier
les noms de variables ne commencent pas par des chiffres
syntaxe de déclaration de fonction :

```intech
fonction addition (entier tmp1, entier tmp2) : entier{retourner 1;}
```

## Fonctionnalités

+ Lexer -> les trois fonctions
+ AST, Symbol, les fonctions capables de travailler avec les structures de données fournies, plus des fonctions d'affichage
+ Parser :
  Lecture d'une fonction, de ses paramètres, de son type de retour, de ses statements
  Retour de la table des symboles d'un script en .intech suivant les règles établies
  Gère : variables, nombres, retours, unary, binary
  Ne gère pas encore : si/sinon, tantque, les compound statements, les appels de fonctions
  Ne lit pas encore : plusieurs fonctions, ni leurs appels
+ Generator :
  Peut traduire en Python la table générée

## Remarques

Utilisation :
compiler les sources et un fichier d'entrée en un exécutable :
les fichiers d'entrées possibles sont :
=> ceux testant un fichier : compoundTest.c, parserTest.c, minusTest.c, unaryTest.c
=> ceux testant une feature : astTest.c, symbolTest.c, bufferTest.c
=> obsolete : lexerTest.c

Le Makefile fournit des instructions pour générer un exécutable basé sur l'une de ces entrées avec gcc
exemple : pour compiler returnMinus.intech, entrer `make minus` dans un terminal, l'exécuter avec `./returnMinus` puis observer le fichier python généré avec `cat returnMinus.py`.

Functions : snake_case
Variables : camelCase
Tabulations : 2; Tabulations pour python : 4

Vocabulary :
statement : a = b+1;
expression : b+1;
declaration : First encounter with a variable, can be assignated right away or not
entier newVariable = 1;
entier otherNewVariable;
assignement : Assign a new value to an existing variable
compound_statement :
{
  b++;
  a = b+1;
}
function body (aka function statements) :
{
  b = param1;
  {
    b++;
    a = b+1;
  }
  retourner a*b;
}
