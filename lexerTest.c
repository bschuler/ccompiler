#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "Source/buffer.h"
#include "Source/utils.h"
#include "Source/lexer.h"

void readfile_nobuffer(FILE* fptr);
char* readword(buffer_t* ptbuf);
void readword_rollback(buffer_t* ptbuf);
void readNumber(buffer_t* ptbuf);

int main()
{
  printf("\nTesting my lexer\n\n");
  buffer_t myBuffer;
  FILE *fptr;
  int loops = 10;
  
  
  fptr = fopen("./crashtest.intech","r");
  buf_init(&myBuffer, fptr);

  readNumber(&myBuffer);
  char** lexemTable = (char **) malloc(LEXEM_SIZE*loops);

  for(int i=0; i<loops; i++){
    lexemTable[i] = readword(&myBuffer);
    readword(&myBuffer);
  }

  // for(int i=0; i<loops; i++){
  //   printf("%s\n", lexemTable[i]);
  // }


  // for(int i=0; i<60; i++){
  //   free(lexemTable[i]);
  // }
  //free(lexemTable[0]);

  buf_print(&myBuffer);


  fclose(fptr);
  printf("\n\nTested my lexer\n\n");
  return 0;
}

char* readword(buffer_t* ptbuf){
  //buf_print(ptbuf);

  char* word = lexer_getalphanum(ptbuf);
  printf("%s\n", word);
  if (!isalnum(word[0])){
     char* output = (char*) malloc(LEXEM_SIZE);
     buf_getnchar(ptbuf, output, 1);
     free(output);
  }
  //printf("%s\n", word);
  
  //buf_print(ptbuf);
  return word;
}

void readword_rollback(buffer_t* ptbuf){
  //buf_print(ptbuf);

  char* word = lexer_getalphanum_rollback(ptbuf);
  printf("%s\n", word);
  
  buf_print(ptbuf);
  free(word);
}

void readNumber(buffer_t* ptbuf){
  buf_print(ptbuf);

  long int number = lexer_getnumber(ptbuf);
  printf("%ld\n", number);
  
  buf_print(ptbuf);
}

void readfile_nobuffer(FILE* fptr){
  char ch;

  if(fptr == NULL)
  {
    printf("Error!");   
    exit(1);             
  }

  while ( 1 )  
  {  
    ch = fgetc ( fptr ) ;  
    if ( ch == EOF )  
    break ;  
    printf("%c",ch) ;  
  }  
}