#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "Source/buffer.h"
#include "Source/utils.h"
#include "Source/lexer.h"

void readfile_nobuffer(FILE* fptr);
void readword(buffer_t* ptbuf);
void readword_rollback(buffer_t* ptbuf);
void readNumber(buffer_t* ptbuf);

int main()
{
  printf("\nTesting my buffer\n\n");
  buffer_t myBuffer;
  FILE *fptr;
  
  
  //fptr = fopen("./copypasta.txt","r");
  fptr = fopen("./crashtest.intech", "r");
  buf_init(&myBuffer, fptr);

  for(int i=0; i<10; i++){
    readword(&myBuffer);
  }

  fclose(fptr);
  printf("\n\nTested my buffer\n\n");
  return 0;
}

void readword(buffer_t* ptbuf){

  char* word = lexer_getalphanum(ptbuf);
  if (word[0] == ' '){
    char symbol = buf_getchar(ptbuf);
    printf("Symbol : %c\n", symbol);
  } else {
    printf("%s\n", word);
  }
  
  //buf_print(ptbuf);
  free(word);
}

void readword_rollback(buffer_t* ptbuf){
  //buf_print(ptbuf);

  char* word = lexer_getalphanum_rollback(ptbuf);
  printf("%s\n", word);
  
  buf_print(ptbuf);
  free(word);
}

void readNumber(buffer_t* ptbuf){
  buf_print(ptbuf);

  long int number = lexer_getnumber(ptbuf);
  printf("%ld\n", number);
  
  buf_print(ptbuf);
}

void readfile_nobuffer(FILE* fptr){
  char ch;

  if(fptr == NULL)
  {
    printf("Error!");   
    exit(1);             
  }

  while ( 1 )  
  {  
    ch = fgetc ( fptr ) ;  
    if ( ch == EOF )  
    break ;  
    printf("%c",ch) ;  
  }  
}