#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Source/ast.h"
#include "Source/buffer.h"
#include "Source/utils.h"
#include "Source/lexer.h"

int main(){

  long longNumber = 12341234156091241;
  ast_t* intAst = ast_new_integer(longNumber);
  print_ast(intAst);
  free(intAst);

  ast_t* varAst = ast_new_variable("force", 42);
  print_ast(varAst);
  free(varAst);
  
  ast_t* binAst = ast_new_binary(PLUS, ast_new_integer(5), ast_new_integer(4));
  print_ast(binAst);
  free(binAst);

  ast_list_t* alisteptr = ast_list_new_node(ast_new_integer(1));
  ast_list_t* aligator = ast_list_add(alisteptr, ast_new_integer(2));
  
  ast_t* fakefncall = ast_new_fncall("myFunctionName", aligator);
  print_ast(fakefncall);
  free(aligator);

  ast_list_t* parameters = ast_list_new_node(ast_new_integer(0));
  ast_list_t* statements = ast_list_new_node(ast_new_integer(0));
  print_ast_list(parameters, 0);
  char* name = "yolo";
  ast_t* myNewFunction = ast_new_function(name, 1, parameters, statements);
  print_ast(myNewFunction);
  free(myNewFunction);
  free(parameters);
  free(statements);

  ast_list_t* stmts = NULL;
  ast_t* integerAst = ast_new_integer(0);
  stmts = ast_list_add(stmts, integerAst);
  print_ast_list(stmts, 0);
  free(stmts);

  printf("\nTested ast\n\n");
  return 0;
}