#include "utils.h"
#include <stdlib.h>
#include <string.h>
#ifndef WIN32
#include <unistd.h>
#include <execinfo.h>
#endif

char *copy_name (char *name)
{
  size_t len = strlen(name) + 1;
  char *out = malloc(sizeof(char) * len);
  strncpy(out, name, len);
  return out;
}

void print_backtrace ()
{
#ifndef WIN32
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  backtrace_symbols_fd(array, size, STDERR_FILENO);
#endif /* WIN32 */
}

void print_error(char* message, buffer_t* ptbuf, int exit_code){
  printf("\nERROR : %s\n", message);
  buf_print(ptbuf);
  printf("ERROR printed\n\n");
  fclose(ptbuf->fd);
  exit(exit_code);
}

void pretty_print_string(char* stringName, char* stringToPrint){
  int iterInt = 0;
  char iterChar =  stringToPrint[iterInt];
  printf("%s : ", stringName);
  do{
    printf("%c, ", iterChar);
    iterInt++;
    iterChar = stringToPrint[iterInt];
  } while(iterChar != '\0');
  printf("\n");
}

void print_indent(int indent){
  for(int i=0; i<indent; i++){
    printf("#");
  }
  printf(" ");
}
