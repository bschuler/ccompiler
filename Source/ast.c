#include "ast.h"
#include "stdlib.h"
#include "string.h"
#include <stdio.h>

ast_t* ast_new_integer (long val){
  ast_t* astptr = (ast_t*) malloc(sizeof(ast_t));
  astptr->type = AST_INTEGER;
  astptr->integer = val;
  return astptr;
}

ast_t* ast_new_variable (char* name, int type){
  ast_t* astptr = (ast_t*) malloc(sizeof(ast_t));
  astptr->type = AST_VARIABLE;
  astptr->var.name = name;
  astptr->var.type = type;
  return astptr;
}

ast_t *ast_new_binary (ast_binary_e op, ast_t *left, ast_t *right){
  ast_t* astptr = (ast_t*) malloc(sizeof(ast_t));
  astptr->type = AST_BINARY;
  astptr->binary.op = op;
  astptr->binary.left = left;
  astptr->binary.right = right;
  return astptr;
}

ast_t *ast_new_unary (ast_binary_e op, ast_t *operand){
  ast_t* astptr = (ast_t*) malloc(sizeof(ast_t));
  astptr->type = AST_UNARY;
  astptr->unary.op = op;
  astptr->unary.operand = operand;
  return astptr;
}

ast_list_t *ast_list_new_node (ast_t *elem){
  ast_list_t* alisteptr = (ast_list_t*) malloc(sizeof(ast_list_t));
  alisteptr->node = elem;
  return alisteptr;
}

ast_list_t *ast_list_add (ast_list_t *list, ast_t *elem){
  //adds the elem at the end of the list and returns the list
  ast_list_t* alisteptr = (ast_list_t*) malloc(sizeof(ast_list_t));
  alisteptr->node = elem;
  if(!list){
    return alisteptr;
  }
  ast_list_t* current = list;
  while(current->next){
    current = current->next;
  }
  current->next = alisteptr;
  return list;
}

ast_t *ast_new_function (char *name, int return_type, ast_list_t *params, ast_list_t *stmts){
  ast_t* astptr = (ast_t*) malloc(sizeof(ast_t));
  astptr->type = AST_FUNCTION;
  astptr->function.name = name;
  astptr->function.return_type = return_type;
  astptr->function.params = params;
  astptr->function.stmts = stmts;
  return astptr;
}

ast_t *ast_new_fncall (char *name, ast_list_t *args){
  ast_t* astptr = (ast_t*) malloc(sizeof(ast_t));
  astptr->type = AST_FNCALL;
  astptr->call.name = name;
  astptr->call.args = args;
  return astptr;
}

ast_t *ast_new_comp_stmt (ast_list_t *stmts){
  ast_t* astptr = (ast_t*) malloc(sizeof(ast_t));
  astptr->type = AST_COMPOUND_STATEMENT;
  astptr->compound_stmt.stmts = stmts;
  return astptr;
}

ast_t *ast_new_assignment (ast_t *lvalue, ast_t *rvalue){
  ast_t* astptr = (ast_t*) malloc(sizeof(ast_t));
  astptr->type = AST_ASSIGNMENT;
  astptr->assignment.lvalue = lvalue;
  astptr->assignment.rvalue = rvalue;
  return astptr;
}

ast_t *ast_new_declaration (ast_t *lvalue, ast_t *rvalue){
  ast_t* astptr = (ast_t*) malloc(sizeof(ast_t));
  astptr->type = AST_DECLARATION;
  astptr->declaration.lvalue = lvalue;
  if(rvalue){
    astptr->declaration.rvalue = rvalue;
  }
  return astptr;
}

ast_t *ast_new_condition (ast_t *condition, ast_t *valid, ast_t *invalid){
  ast_t* astptr = (ast_t*) malloc(sizeof(ast_t));
  astptr->type = AST_CONDITION;
  astptr->branch.condition = condition;
  astptr->branch.valid = valid;
  astptr->branch.invalid = invalid;
  return astptr;
}

ast_t *ast_new_loop (ast_t *condition, ast_t *stmt){
  ast_t* astptr = (ast_t*) malloc(sizeof(ast_t));
  astptr->type = AST_LOOP;
  astptr->loop.condition = condition;
  astptr->loop.stmt = stmt;
  return astptr;
}

ast_t *ast_new_return (ast_t *expr){
  ast_t* astptr = (ast_t*) malloc(sizeof(ast_t));
  astptr->type = AST_RETURN;
  astptr->ret.expr = expr;
  return astptr;
}

void print_ast_node_type(ast_node_type_e astNodeType){
  switch (astNodeType)
  {
  case 0:
    printf("AST_VOID\n");
    break;
  case 1:
    printf("AST_INTEGER\n");
    break;
  case 2:
    printf("AST_BINARY\n");
    break;
  case 3:
    printf("AST_UNARY\n");
    break;
  case 4:
    printf("AST_FUNCTION\n");
    break;
  case 5:
    printf("AST_FNCALL\n");
    break;
  case 6:
    printf("AST_VARIABLE\n");
    break;
  case 7:
    printf("AST_CONDITION\n");
    break;
  case 8:
    printf("AST_LOOP\n");
    break;
  case 9:
    printf("AST_DECLARATION\n");
    break;
  case 10:
    printf("AST_ASSIGNMENT\n");
    break;
  case 11:
    printf("AST_COMPOUND_STATEMENT\n");
    break;
  case 12:
    printf("AST_RETURN\n");
    break;
  default:
    printf("AstNodeType error\n");
    break;
  }
}

void printOperatorType(ast_binary_e operator){
  printf("OPERATOR : ");
  switch (operator)
  {
  case 0:
    printf("+");
    break;
  case 1:
    printf("-");
    break;
   case 2:
    printf("*");
    break;
  case 3:
    printf("/");
    break;
  case 4:
    printf("==");
    break;
  case 5:
    printf("!=");
    break;
  case 6:
    printf(">");
    break;
  case 7:
    printf("<");
    break;
  case 8:
    printf(">=");
    break;
  case 9:
    printf("<=");
    break;
  case 10:
    printf("&&");
    break;
  case 11:
    printf("||");
    break;
  case 12:
    printf("++");
    break;
  case 13:
    printf("--");
    break;
  default:
    printf("AstBinaryType error");
    break;
  }
  printf("\n");
}

void print_ast_with_indent(ast_t* ast, int indent){
  print_indent(indent);
  switch (ast->type)
  {
  case 0:
    printf("AST_VOID\n");
    break;
  case 1:
    printf("AST_INTEGER\n");
    print_indent(indent);
    printf("%ld\n",ast->integer);
    break;
  case 2:
    printf("AST_BINARY\n");
    print_indent(indent);
    printOperatorType(ast->binary.op);
    print_ast_with_indent(ast->binary.left, indent+1);
    print_ast_with_indent(ast->binary.right, indent+1);
    break;
  case 3:
    printf("AST_UNARY\n"); //op not distinguished with binary op
    print_indent(indent);
    printOperatorType(ast->unary.op);
    print_ast_with_indent(ast->unary.operand, indent+1);
    break;
  case 4:
    printf("AST_FUNCTION\n");
    print_indent(indent);
    printf("%s\n",ast->function.name);
    print_indent(indent);
    printf("%d\n",ast->function.return_type);
    print_indent(indent);
    if(ast->function.params){
      printf("FUNCTION PARAMETERS :\n");
      print_ast_list(ast->function.params, indent+1);
    } else{
      printf("FUNCTION WITHOUT PARAMETERS\n");
    }
    print_indent(indent);
    printf("FUNCTION STATEMENTS :\n");
    print_ast_list(ast->function.stmts, indent+1);
    break;
  case 5:
    printf("AST_FNCALL\n");
    printf("%s\n",ast->call.name);
    print_ast_list(ast->call.args, indent+1);
    break;
  case 6:
    printf("AST_VARIABLE\n");
    print_indent(indent);
    printf("Name : %s\n",ast->var.name);
    print_indent(indent);
    printf("Type : %d\n",ast->var.type);
    break;
  case 7:
    printf("AST_CONDITION\n");
    print_indent(indent);
    printf("Conditons : \n");
    print_ast_with_indent(ast->branch.condition, indent+1);
    print_indent(indent);
    printf("Valid if : \n");
    print_ast_with_indent(ast->branch.valid, indent+1);
    print_indent(indent);
    printf("Invalid if : \n");
    print_ast_with_indent(ast->branch.invalid, indent+1);
    break;
  case 8:
    printf("AST_LOOP\n");
    print_indent(indent);
    printf("Conditons : \n");
    print_ast_with_indent(ast->loop.condition, indent+1);
    print_indent(indent);
    printf("Statements : \n");
    print_ast_with_indent(ast->loop.stmt, indent+1);
    break;
  case 9:
    printf("AST_DECLARATION\n");
    print_indent(indent);
    printf("Left value : \n");
    print_ast_with_indent(ast->declaration.lvalue, indent+1);
    if(ast->declaration.rvalue){
      print_indent(indent);
      printf("Right value : \n");
      print_ast_with_indent(ast->declaration.rvalue, indent+1);
    }
    break;
  case 10:
    printf("AST_ASSIGNMENT\n");
    print_indent(indent);
    printf("Left value : \n");
    print_ast_with_indent(ast->assignment.lvalue, indent+1);
    print_indent(indent);
    printf("Right value : \n");
    print_ast_with_indent(ast->assignment.rvalue, indent+1);
    break;
  case 11:
    printf("AST_COMPOUND_STATEMENT\n");
    print_ast_list(ast->compound_stmt.stmts, indent+1);
    break;
  case 12:
    printf("AST_RETURN\n");
    print_ast_with_indent(ast->ret.expr, indent+1);
    break;
  default:
    printf("AstNodeType error\n");
    break;
  }
}

void print_ast_list(ast_list_t* astlist, int indent){
  print_indent(indent);
  printf("Printing new Astlist\n");
  print_ast_list_with_indent(astlist, indent);
}

void print_ast_list_with_indent(ast_list_t* astlist, int indent){
  print_ast_with_indent(astlist->node, indent);
  if(astlist->next != NULL){
    print_ast_list_with_indent(astlist->next, indent);
  }
  else{
    print_indent(indent);
    printf("Astlist printed\n");
  }
}

void print_ast(ast_t* ast){
  printf("\n");
  printf("Printing new ast\n");
  print_ast_with_indent(ast, 1);
}