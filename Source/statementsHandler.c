#include "statementsHandler.h"

#include "string.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stddef.h>
#include <ctype.h>


ast_list_t* handle_declaration(buffer_t* ptbuf, ast_list_t* statements, symbol_t** ptLocalSymbolTable, symbol_t* globalSymbolTable){
  lexer_getalphanum(ptbuf);
  char* declaredVariableName = lexer_getalphanum(ptbuf);
  ensure_variable_name_for_both_symtables(ptbuf, declaredVariableName,*ptLocalSymbolTable,globalSymbolTable);
  ast_t* variableAst = ast_new_variable(declaredVariableName, 1);
  ast_t* expressionAst = pre_expression(ptbuf, *ptLocalSymbolTable, globalSymbolTable);
  ast_t* declarationAst = ast_new_declaration(variableAst, expressionAst);
  *ptLocalSymbolTable = sym_add(*ptLocalSymbolTable, sym_new(declaredVariableName, SYM_VAR, variableAst));
  return ast_list_add(statements, declarationAst);
}

ast_list_t* handle_assignement(buffer_t* ptbuf, ast_list_t* statements, symbol_t* symbolFoundInLocalTable, symbol_t* localSymbolTable, symbol_t* globalSymbolTable){
    ast_t* variableAst = ast_new_variable(symbolFoundInLocalTable->attributes->var.name, 1);
    ast_t* expressionAst = pre_expression(ptbuf, localSymbolTable, globalSymbolTable);
    if(expressionAst){
        ast_t* assignmentAst = ast_new_assignment(variableAst, expressionAst);
        return ast_list_add(statements, assignmentAst);
    } else{
        return ast_list_add(statements, variableAst);
    }
}