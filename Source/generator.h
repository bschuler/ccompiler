#ifndef GENERATOR_H
#include "parserhelper.h"

void generate(symbol_t* globalSymTable, char* filename);
ast_t* translateAst(FILE* fptr, ast_t* ast);
symbol_t* translateFunction(FILE* fptr, symbol_t* globalSymTable);
void translateParams(FILE* fptr, ast_list_t* parametersList);
void py_print(FILE* fptr, int indent);
void translateOperator(FILE* fptr, ast_binary_e op);

#define GENERATOR_H
#endif