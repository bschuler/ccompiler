#include "parserhelper.h"
#include "string.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stddef.h>
#include <ctype.h>

void ensure_variable_name(buffer_t* ptbuf, char* variableName){
  if(variableName[0] == ' ' || isdigit(variableName[0])){
    print_error("Variable name shall start with a letter", ptbuf, 1);
  }
  char languageSymbols[LANGUAGE_SYMBOLS][LANGUAGE_SYMBOLS_SIZE] = {
    "tantque",
    "si",
    "sinon",
    "fonction",
    "retourner",
    "entier",
    "rien",
    "ET",
    "OU"
  };
  for (int index=0; index<LANGUAGE_SYMBOLS; index++){
    if(strcmp(variableName, languageSymbols[index]) == 0){
      print_error("Variable name was can't be a primitive symbol of the language", ptbuf, 1);
    }
  }
}

void ensure_variable_name_for_target_symtable(buffer_t* ptbuf, char* variableName, symbol_t* symbolTable){
  ensure_variable_name(ptbuf, variableName);
  if(sym_search(symbolTable, variableName)){
    print_error("Variable name was already taken", ptbuf, 1);
  }
}

void ensure_variable_name_for_both_symtables(buffer_t* ptbuf, char* variableName, symbol_t* localSymbolTable, symbol_t* globalSymbolTable){
  ensure_variable_name(ptbuf, variableName);
  if(sym_search(localSymbolTable, variableName)){
    print_error("Variable name was already taken in the function or parameters", ptbuf, 1);
  }
  if(sym_search(globalSymbolTable, variableName)){
    print_error("Variable name was already taken in the script", ptbuf, 1);
  }
}


void ensure_opening_bracket(buffer_t* ptbuf){
  char openingBracket = buf_getchar_after_blank(ptbuf);
  if(openingBracket != '{'){
    print_error("Function shall start with {",ptbuf,1);
  }
}

void ensure_closing_bracket(buffer_t* ptbuf){
  char closingBracket = buf_getchar_after_blank(ptbuf);
  if(closingBracket != '}'){
    print_error("Function shall end with }",ptbuf,1);
  }
}

void ensure_opening_parenthesis(buffer_t* ptbuf){
  char openingParenthesis = buf_getchar_after_blank(ptbuf);
  if(openingParenthesis != '{'){
    print_error("Function shall start with (",ptbuf,1);
  }
}


void ensure_closing_parenthesis(buffer_t* ptbuf){
  char closingParenthesis = buf_getchar_after_blank(ptbuf);
  if(closingParenthesis != '}'){
    print_error("Function shall end with )",ptbuf,1);
  }
}

void ensure_semicolon(buffer_t* ptbuf){
  char closingSemicolon = buf_getchar_after_blank(ptbuf);
  if(closingSemicolon != ';'){
    print_error("Expression shall end with semicolon ;",ptbuf,1);
  }
}

void ensure_variable_type_is_integer(buffer_t* ptbuf, char* variableType){
  if(strcmp(variableType,"entier") != 0){
    print_error("Variable type shall be entier", ptbuf, 1);
  }
}