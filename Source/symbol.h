#ifndef SYMBOL_H
#include "ast.h"

typedef enum
{
  SYM_FUNCTION,
  SYM_VAR,
  SYM_PARAM
} sym_type_t;

typedef struct symbol_t
{
  char *name;
  sym_type_t type;
  ast_t *attributes;
  struct symbol_t *function_table; // for functions ; char ** ?
  struct symbol_t *next;
} symbol_t;

symbol_t *sym_new(char *name, sym_type_t type, ast_t *attributes);
symbol_t *sym_search(symbol_t *table, char *name);
symbol_t *sym_add(symbol_t *table, symbol_t *sym);
void sym_delete(symbol_t *sym);                  // removes a symbol, called by remove
void sym_remove(symbol_t *table, symbol_t *sym); // removes a symbol in the table
void print_sym_type(sym_type_t symType);
void print_symbol(symbol_t *symbol);

#define SYMBOL_H
#endif