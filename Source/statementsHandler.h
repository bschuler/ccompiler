#ifndef STATEMENTS_HANDLER_H

#include "parserhelper.h"
#include "expression.h"

ast_list_t* handle_declaration(buffer_t* ptbuf, ast_list_t* statements, symbol_t** ptLocalSymbolTable, symbol_t* globalSymbolTable);
ast_list_t* handle_assignement(buffer_t* ptbuf, ast_list_t* statements, symbol_t* symbolFoundInLocalTable, symbol_t* localSymbolTable, symbol_t* globalSymbolTable);

#define STATEMENTS_HANDLER_H
#endif