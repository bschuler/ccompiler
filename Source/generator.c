#include "generator.h"
#include <stdlib.h>
#include "string.h"
#include "stdio.h"
#include "symbol.h"

void generate(symbol_t* globalSymTable, char* filename){

  if(!globalSymTable){
    printf("No symbol table found\n");
    exit(1);
  }
  if(!filename){
    filename = "file";
  }
  char* filename_with_extension = (char*)malloc(LEXEM_SIZE + 3);
  strcpy(filename_with_extension, filename);
  char* extension = ".py";
  strcat(filename_with_extension, extension);

  FILE* fptr = fopen(filename_with_extension, "w");
  if (fptr == NULL){
    printf("ERROR while creating the file %s\n", filename_with_extension);
    exit(1);
  }

  fprintf(fptr, "#/usr/bin/python3\n\n");

  while(globalSymTable){
    globalSymTable = translateFunction(fptr, globalSymTable);
  }
}

symbol_t* translateFunction(FILE* fptr, symbol_t* globalSymTable){
  fprintf(fptr, "def ");
  ast_t* functionAst = globalSymTable->attributes;
  fprintf(fptr, functionAst->function.name);
  fprintf(fptr," (");
  ast_list_t* functionParameters = functionAst->function.params;
  if(functionParameters){
    translateParams(fptr, functionParameters);
  }
  fprintf(fptr,"):\n");
  ast_list_t* functionStatements = functionAst->function.stmts;
  while(functionStatements){
    py_print(fptr, 1);
    translateAst(fptr, functionStatements->node);
    functionStatements = functionStatements->next;
  }

  fprintf(fptr, "\n");
  return globalSymTable->next;
}

void py_print(FILE* fptr, int indent){
  for(int i=0; i<indent; i++){
    fprintf(fptr, "    ");
  }
}

void translateParams(FILE* fptr, ast_list_t* parametersList){
  while(parametersList){
    ast_t* parametersAst = parametersList->node;
    fprintf(fptr, parametersAst->var.name);
    if(parametersList->next){
      fprintf(fptr, " ,");
    }
    parametersList = parametersList->next;
  }
}

ast_t* translateAst(FILE* fptr, ast_t* ast){
  switch (ast->type)
  {
  case AST_VARIABLE:
    fprintf(fptr, ast->var.name);
    break;
  case AST_INTEGER:
    fprintf(fptr, "%ld", ast->integer);
    break;
  case AST_RETURN:
    fprintf(fptr, "return ");
    translateAst(fptr, ast->ret.expr);
    fprintf(fptr, "\n");    break;
  case AST_UNARY:
    translateAst(fptr, ast->unary.operand);
    translateOperator(fptr, ast->unary.op);
    break;
  case AST_DECLARATION:
    translateAst(fptr, ast->declaration.lvalue);
    fprintf(fptr, " = ");
    if(ast->declaration.rvalue){
      translateAst(fptr, ast->declaration.rvalue);
    } else {
      fprintf(fptr, "None");
    }
    fprintf(fptr, "\n");
    break;
  case AST_BINARY:
    translateAst(fptr, ast->binary.left);
    translateOperator(fptr, ast->binary.op);
    translateAst(fptr, ast->binary.right);
    break;
  case AST_ASSIGNMENT:
    translateAst(fptr, ast->declaration.lvalue);
    fprintf(fptr, " = ");
    translateAst(fptr, ast->declaration.rvalue);
    fprintf(fptr, "\n");
    break;
  
  default:
    break;
  }
}

void translateOperator(FILE* fptr, ast_binary_e op){
  switch(op){
    case PLUS:
      fprintf(fptr, " + ");
      break;
    case MINUS:
      fprintf(fptr, " - ");
      break;
    case MULT:
      fprintf(fptr, " * ");
      break;
    case DIV:
      fprintf(fptr, " / ");
      break;
    case PLUSPLUS:
      fprintf(fptr, "++");
      break;
    case MINUSMINUS:
      fprintf(fptr, "--");
      break;
  }
}