#include "expression.h"
#include <stdlib.h>
#include <ctype.h>

ast_t* pre_expression(buffer_t* ptbuf, symbol_t* localSymbolTable, symbol_t* globalSymbolTable){ 
  char postVariableSymbol = buf_getchar_after_blank(ptbuf);
  if(postVariableSymbol != ';'){
    if(postVariableSymbol == '='){
      ast_t* expressionAst = expression_analysis(ptbuf, localSymbolTable, globalSymbolTable);
      return expressionAst;
    } else {
      print_error("Statement shall end by ;", ptbuf,1);
    }
  }
  return NULL;
}

ast_t* expression_analysis(buffer_t* ptbuf, symbol_t* localSymbolTable, symbol_t* globalSymbolTable){
  char* nextElement = lexer_getalphanum_rollback(ptbuf);
  //pretty_print_string("next elt", nextElement);
  ast_t* expressionAst = NULL;

  if(nextElement[0] == ' ') {
    print_error("No expression shall start with a symbol", ptbuf,1);
  }
  else if(isdigit(nextElement[0])) {
    //check that it is a full number -> or not, it will then fail later
    expressionAst = ast_new_integer(lexer_getnumber(ptbuf));
  }
  else if(sym_search(localSymbolTable, nextElement)){
    //printf("I'm in the table\n");
    lexer_getalphanum(ptbuf);
    //fncall or work with variable;
    //variable here
    expressionAst = ast_new_variable(nextElement, 1);
  }
  else if(sym_search(globalSymbolTable, nextElement)){
    lexer_getalphanum(ptbuf);
    //work with fncall variable;
  }
  else{
    print_error("The compiler could not understand this expression",ptbuf,1);
  }

  //operators
  //no operator precedence
  char operator = buf_getchar_after_blank(ptbuf);
  //printf("operator : %c\n",operator);
  switch(operator){
    case ';':
      return expressionAst;
      break;
    case '+':
      return plus_right_member(ptbuf, expressionAst, localSymbolTable, globalSymbolTable);
      break;
    case '-':
      return minus_right_member(ptbuf, expressionAst, localSymbolTable, globalSymbolTable);
      break;
    case '*':
      return mult_right_member(ptbuf, expressionAst, localSymbolTable, globalSymbolTable);
      break;
    case '/':
      return div_right_member(ptbuf, expressionAst, localSymbolTable, globalSymbolTable);
      break;
    default:
      print_error("operator not understood", ptbuf, 1);
      break;
  }

}


ast_t* plus_right_member(buffer_t* ptbuf,ast_t* expressionAst,symbol_t* localSymbolTable,symbol_t* globalSymbolTable){
  char isItUnary = buf_getchar_rollback(ptbuf);
  if(isItUnary == '+'){
    buf_getchar_after_blank(ptbuf);
    ast_t* unaryAst = ast_new_unary(PLUSPLUS, expressionAst);

    ensure_semicolon(ptbuf);
    return unaryAst;
  }
  else {
    ast_t* rightMember = expression_analysis(ptbuf, localSymbolTable, globalSymbolTable);
    return ast_new_binary(PLUS, expressionAst, rightMember);
  }
}


ast_t* minus_right_member(buffer_t* ptbuf,ast_t* expressionAst,symbol_t* localSymbolTable,symbol_t* globalSymbolTable){
  char isItUnary = buf_getchar_rollback(ptbuf);
  if(isItUnary == '-'){
    buf_getchar_after_blank(ptbuf);
    ast_t* unaryAst = ast_new_unary(MINUSMINUS, expressionAst);

    ensure_semicolon(ptbuf);
    return unaryAst;
  }
  else {
    ast_t* rightMember = expression_analysis(ptbuf, localSymbolTable, globalSymbolTable);
    return ast_new_binary(MINUS, expressionAst, rightMember);
  }
}

ast_t* mult_right_member(buffer_t* ptbuf,ast_t* expressionAst,symbol_t* localSymbolTable,symbol_t* globalSymbolTable){
  ast_t* rightMember = expression_analysis(ptbuf, localSymbolTable, globalSymbolTable);
  return ast_new_binary(MULT, expressionAst, rightMember);
}

ast_t* div_right_member(buffer_t* ptbuf,ast_t* expressionAst,symbol_t* localSymbolTable,symbol_t* globalSymbolTable){
  ast_t* rightMember = expression_analysis(ptbuf, localSymbolTable, globalSymbolTable);
  return ast_new_binary(DIV, expressionAst, rightMember);
}
