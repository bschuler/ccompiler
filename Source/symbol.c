#include "symbol.h"
#include "string.h"
#include "utils.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stddef.h>

symbol_t* sym_new (char *name, sym_type_t type, ast_t *attributes){
  symbol_t* symbolptr = (symbol_t*) malloc(sizeof(symbol_t));
  symbolptr->name = name;
  symbolptr->type = type;
  symbolptr->attributes = attributes;
  return symbolptr;
}

symbol_t* sym_add (symbol_t* table, symbol_t* sym){
  if(!table){
    return sym;
  }
  symbol_t* current = table;
  while(current->next){
    current = current->next;
  }
  current->next = sym;
  return table;
}

void sym_delete(symbol_t* sym){
  free(sym->attributes);
  free(sym);
}

void sym_remove(symbol_t* table, symbol_t* sym){
  symbol_t* current = table;
  if(table == sym){
    sym_delete(sym);
  }
  while(current->next){
    if (current->next == sym){
      break;
    }
    current = current->next;
  }
  if(current->next){
    current->next = current->next->next;
    sym_delete(current->next);
  }
}

symbol_t* sym_search(symbol_t* table, char* name){
  if(!table){
    return NULL;
  }
  pretty_print_string("name", name);
  pretty_print_string("table->name", table->name);
  if(strcmp(table->name, name) == 0){
    return table;
  }
  while(table->next){
    table = table->next;
    if(strcmp(table->name, name) == 0){
      return table;
    }
  }
  return NULL;
}

void print_sym_type(sym_type_t symType){
  printf("Symbol type : ");
  switch (symType)
  {
  case 0:
    printf("SYM_FUNCTION\n");
    break;
  case 1:
    printf("SYM_VAR\n");
    break;
  case 2:
    printf("SYM_PARAM\n");
    break;
  default:
    printf("SymType error\n");
    break;
  }
}

void print_symbol(symbol_t* symbol){
  if(!symbol){
    printf("Symbol is empty\n");
    return;
  }
  printf("\n$ Printing new symbol\n");
  print_indent(1);
  printf("Symbol name : %s\n",symbol->name);
  print_indent(1);
  print_sym_type(symbol->type);
  print_ast_with_indent(symbol->attributes, 2);
  // if(symbol->function_table){
  //   printf("Symbol has a function table.\n");
  //   print_symbol(symbol->function_table);
  // }
  if(symbol->next){
    print_indent(1);
    printf("Next symbol is %s\n",symbol->next->name);
  }
  else{
    print_indent(1);
    printf("No next symbol\n");
  }
  printf("$ symbol printed\n\n");
}
