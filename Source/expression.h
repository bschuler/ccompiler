#ifndef EXPRESSION_H
#include "parserhelper.h"

ast_t* pre_expression(buffer_t* ptbuf, symbol_t* localSymbolTable, symbol_t* globalSymbolTable);
ast_t* expression_analysis(buffer_t* ptbuf, symbol_t* localSymbolTable, symbol_t* globalSymbolTable);

ast_t* plus_right_member(buffer_t* ptbuf,ast_t* expressionAst,symbol_t* localSymbolTable,symbol_t* globalSymbolTable);
ast_t* minus_right_member(buffer_t* ptbuf,ast_t* expressionAst,symbol_t* localSymbolTable,symbol_t* globalSymbolTable);
ast_t* mult_right_member(buffer_t* ptbuf,ast_t* expressionAst,symbol_t* localSymbolTable,symbol_t* globalSymbolTable);
ast_t* div_right_member(buffer_t* ptbuf,ast_t* expressionAst,symbol_t* localSymbolTable,symbol_t* globalSymbolTable);

#define EXPRESSION_H
#endif