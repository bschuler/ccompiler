#ifndef PARSERHELPER_H

#include "symbol.h"
#include "lexer.h"
#define LANGUAGE_SYMBOLS 9
#define LANGUAGE_SYMBOLS_SIZE 10

void ensure_variable_name(buffer_t* ptbuf, char* variableName);
void ensure_variable_name_for_target_symtable(buffer_t* ptbuf, char* variableName, symbol_t* symbolTable);
void ensure_variable_name_for_both_symtables(buffer_t* ptbuf, char* variableName, symbol_t* localSymbolTable, symbol_t* globalSymbolTable);
void ensure_opening_bracket(buffer_t* ptbuf);
void ensure_closing_bracket(buffer_t* ptbuf);
void ensure_opening_parenthesis(buffer_t* ptbuf);
void ensure_closing_parenthesis(buffer_t* ptbuf);
void ensure_semicolon(buffer_t* ptbuf);
void ensure_variable_type_is_integer(buffer_t* ptbuf, char* variableType);

#define PARSERHELPER_H
#endif