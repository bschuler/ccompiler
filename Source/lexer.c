#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include "lexer.h"

char * lexer_getalphanum(buffer_t * buffer){

  buf_lock(buffer);

  char* str = (char*) malloc(LEXEM_SIZE);
  int i = 0;
  char lastChar = buf_getchar_after_blank(buffer);
  if(!isalnum(lastChar)){
    buf_rollback_and_unlock(buffer, 1);
    str[0] = ' ';
    return str;
  }
  while(isalnum(lastChar)){
    str[i] = lastChar;
    i++;
    lastChar = buf_getchar(buffer);
    if (i>=LEXEM_SIZE-1){
      // there wont be bigger elements, but if there is we cut
      break;
    }
  }
  str[i]='\0';
  buf_rollback_and_unlock(buffer, 1); //he read the next unwanted char
  return str;
}

char * lexer_getalphanum_rollback(buffer_t * buffer){

  buf_lock(buffer);

  char* str = (char*) malloc(LEXEM_SIZE);
  int i = 0;
  char lastChar = buf_getchar_after_blank(buffer);
  if(!isalnum(lastChar)){
    buf_rollback_and_unlock(buffer, 1);
    str[0] = ' ';
    return str;
  }
  while(isalnum(lastChar)){
    str[i] = lastChar;
    i++;
    lastChar = buf_getchar(buffer);
    if (i>=LEXEM_SIZE-1){
      break;
    }
  }
  str[i]='\0';
  buf_rollback_and_unlock(buffer, i+1);
  return str;
}

long lexer_getnumber (buffer_t * buffer){

  buf_lock(buffer);

  char* str = (char*) malloc(LEXEM_SIZE);
  int i = 0;
  char lastChar = buf_getchar_after_blank(buffer);
  // if(lastChar == '-'){
  //   str[i] = lastChar;
  //   i++;
  //   char lastChar = buf_getchar(buffer);
  // }
  // // no negative numbers for now
  if(!isdigit(lastChar)){
    buf_rollback_and_unlock(buffer, 1);
    return -1;
  }
  while(isdigit(lastChar)){
    str[i] = lastChar;
    i++;
    lastChar = buf_getchar(buffer);
    if (i>=LEXEM_SIZE){
      break;
    }
  }
  char* remains;
  long int number = strtol(str, &remains, 10);
  free(str);
  buf_rollback_and_unlock(buffer, 1);
  return number;
}
