#include "parser.h"

#include "string.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stddef.h>
#include <ctype.h>


symbol_t* parser(char* filename){

  buffer_t myBuffer;
  FILE *fptr;
  char* filename_with_extension = (char*)malloc(LEXEM_SIZE + 7);
  strcpy(filename_with_extension, filename);
  char* extension = ".intech";
  strcat(filename_with_extension, extension);

  fptr = fopen(filename_with_extension, "r");
  if (fptr == NULL){
    printf("ERROR : no file with name %s\n", filename_with_extension);
  }
  buf_init(&myBuffer, fptr);

  ast_t* myFunctionAst = NULL;
  symbol_t* globalFunctionsSymtable = NULL;
  
  do{
    char* functionKeyword = lexer_getalphanum(&myBuffer);
    if(strcmp(functionKeyword,"fonction") != 0){
      print_error("First lexem shall be fonction", &myBuffer, 1);
    }

    myFunctionAst = function_analysis(&myBuffer);
    symbol_t* functionSymbol = sym_new(myFunctionAst->function.name, SYM_FUNCTION, myFunctionAst);
    print_symbol(functionSymbol);
    globalFunctionsSymtable = sym_add(globalFunctionsSymtable, functionSymbol);

  } while(!buf_eof(&myBuffer));

  fclose(fptr);

  return globalFunctionsSymtable;
}


ast_t* function_analysis(buffer_t* ptbuf){

  char* functionName = lexer_getalphanum(ptbuf);
  if (functionName[0] == ' ' || isdigit(functionName[0])){
    print_error("Function shall start with a letter", ptbuf, 1);
  } 
  //TODO check if function name isn't a language element
  //TODO check if function name is not already taken by another defined function

  ast_list_t* parameters = parameters_analysis(ptbuf);
  int return_type = return_type_analysis(ptbuf);
  ast_list_t* statements = body_analysis(ptbuf, parameters, NULL);

  ast_t* returningFunction = ast_new_function(functionName, return_type, parameters, statements);
  printf("returning function\n");
  return returningFunction;
}


ast_list_t* parameters_analysis(buffer_t* ptbuf){

  char openingParenthesis = buf_getchar_after_blank(ptbuf);
  if (openingParenthesis != '('){
    print_error("Function parameters shall be opened by parenthesis", ptbuf, 1);
  }
  char* variableType;
  char* variableName;
  char separator;
  ast_list_t* parametersList = NULL;

  char closingParenthesis = buf_getchar_rollback(ptbuf);
  if (closingParenthesis == ')'){
    buf_getchar(ptbuf);
    return NULL;
  }

  char* isItAVariable = lexer_getalphanum_rollback(ptbuf);
  do {
    variableType = lexer_getalphanum(ptbuf);
    ensure_variable_type_is_integer(ptbuf, variableType);
    variableName = lexer_getalphanum(ptbuf);
    ensure_variable_name(ptbuf, variableName);
    parametersList = ast_list_add(parametersList, ast_new_variable(variableName, 1)); //only ints, ints will be 1

    separator = buf_getchar_after_blank(ptbuf);
  } while (separator == ',');

  if (separator != ')'){
    print_error("Function shall be terminated by )", ptbuf, 1);
  }
  free(variableType);
  return parametersList;
}


int return_type_analysis(buffer_t* ptbuf){
  char doublePoint =  buf_getchar_after_blank(ptbuf);
  if(doublePoint != ':'){
    print_error("Function shall have : after parameters declaration", ptbuf, 1);
  }
  char* returnType = lexer_getalphanum(ptbuf);
  int returnTypeInt;

  if(strcmp(returnType,"entier") == 0){
    returnTypeInt = 1;
  } else if (strcmp(returnType,"rien") == 0){
    returnTypeInt = 0;
  } else {
    print_error("Return type shall to be one of the supported return types : entier or rien", ptbuf,1);
  }

  free(returnType);
  return returnTypeInt;
}


ast_list_t* body_analysis(buffer_t* ptbuf, ast_list_t* parameters, symbol_t* globalSymbolTable){
  
  symbol_t* localSymbolTable = NULL;

  //create symbol table containing given parameters
  if(parameters){
    ast_t* firstParameter = parameters->node;
    localSymbolTable = sym_new(firstParameter->var.name, firstParameter->var.type, firstParameter);
    while(parameters->next){
      parameters = parameters->next;
      ast_t* parameter = parameters->node;
      localSymbolTable = sym_add(localSymbolTable, sym_new(parameter->var.name, parameter->var.type, parameter));
    }
  };

  //deal with compound statement until } => we can return several times but it's just dead code
  ensure_opening_bracket(ptbuf);

  ast_list_t* statements = NULL;
  char* nextElement;

  do{
    nextElement = lexer_getalphanum_rollback(ptbuf);
    //printf("next elt : %s\n",nextElement);

    if(strcmp(nextElement, "si") == 0){
      //handle IF/ELSE
    }
    else if (strcmp(nextElement, "tantque") == 0){
      //handle tantque
    }
    else if (strcmp(nextElement, "retourner") == 0){
      lexer_getalphanum(ptbuf);
      ast_t* returningExpression = expression_analysis(ptbuf, localSymbolTable, globalSymbolTable);
      ast_t* returnedAst = ast_new_return(returningExpression);
      statements = ast_list_add(statements, returnedAst);
    }
    else if (strcmp(nextElement, "fonction") == 0){
      ast_t* newFunction = function_analysis(ptbuf);
      //TODO
    }
    else if(nextElement[0] == ' ') {
      char symbol = buf_getchar_after_blank(ptbuf);
      if (symbol == '}'){
        break; //end of function !
      }
      else if (symbol == '{'){
        //analyse compound statement
      }
      else {
        print_error("Compiler did not understand the symbol", ptbuf,1);
      }
    }
    else if(isdigit(nextElement[0])) {
      print_error("No line shall start with a number", ptbuf,1);
    }
    else if(strcmp(nextElement, "entier") == 0){
      statements = handle_declaration(ptbuf, statements, &localSymbolTable, globalSymbolTable);
    }
    else {
      lexer_getalphanum(ptbuf);          
      //fncall or assignation
      symbol_t* symbolInLocalTable = sym_search(localSymbolTable, nextElement);
      if(symbolInLocalTable){
        switch(symbolInLocalTable->type){
          case SYM_FUNCTION:
            //fncall
            break;
          case SYM_PARAM:
            statements = handle_assignement(ptbuf, statements, symbolInLocalTable, localSymbolTable, globalSymbolTable);
            break;
          case SYM_VAR:
            statements = handle_assignement(ptbuf, statements, symbolInLocalTable, localSymbolTable, globalSymbolTable);
            break;
          default:
            print_error("incorrect symbol",ptbuf,1);
            break;
        }
      } else {
        symbol_t* symbolInGlobalTable = sym_search(globalSymbolTable, nextElement);
        if(symbolInGlobalTable){
          //fncall
        }
        else{
          print_error("The compiler could not understand the start of this statement",ptbuf,1);
        }
      }
    }
  } while (true); // leaves on }

  return statements;
}
