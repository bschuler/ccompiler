#ifndef PARSER_H

#include "generator.h"
#include "expression.h"
#include "statementsHandler.h"

symbol_t* parser(char* filename);
ast_t* function_analysis(buffer_t* ptbuf);
ast_list_t* parameters_analysis(buffer_t* ptbuf);
int return_type_analysis(buffer_t* ptbuf);
ast_list_t* body_analysis(buffer_t* ptbuf, ast_list_t* parametersTable, symbol_t* globalSymbolTable);

#define PARSER_H
#endif