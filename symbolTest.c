#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Source/symbol.h"



int main(){
  long longNumber = 314159265359;
  ast_t* intAst = ast_new_integer(longNumber);
  printf("%d\n",intAst->type);
  printf("%ld\n",intAst->integer);

  symbol_t* sym1 = sym_new("pie10", SYM_VAR, intAst);
  print_symbol(sym1);

  int age = 18;
  ast_t* varAst = ast_new_variable("age", 18);
  symbol_t* sym2 = sym_new("age", SYM_VAR, varAst);
  sym1 = sym_add(sym1, sym2);

  symbol_t* ageSearched = sym_search(sym1, "age");
  if(ageSearched != NULL){
    printf("agesearched\n");
    print_symbol(ageSearched);
  }
  sym_remove(sym1, sym1);
  //print_symbol(sym1);
  //print_symbol(sym2);
  //print_symbol(sym1->next);
  //sym_delete(sym1);
  //print_symbol(sym2);
  print_ast(intAst);
  print_ast(varAst);
  sym_delete(sym2);
  print_ast(varAst);


  //an empty symboltable can add new member
  symbol_t* emptySymbol = NULL;
  ast_t* varAst3 = ast_new_variable("diplom", 26);
  symbol_t* sym3 = sym_new("age", SYM_VAR, varAst3);
  emptySymbol = sym_add(emptySymbol, sym3);
  print_symbol(emptySymbol);

  return 0;
}